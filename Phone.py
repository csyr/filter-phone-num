#!/usr/bin/python
# -*- coding: UTF8 -*-

import urllib2, socket, re;
from PhoneRule import PhoneRule
from SMTPMail import SMTPMail
from UrlRequest import UrlRequest


class Phone:
    __URL = 'http://num.10010.com/NumApp/chseNumList/serchNums?province=%d&cityCode=%d&sortType=numAsc&Show4GNum=TRUE&goodsNet=4'
    __city_code = {'sh': (31, 310), 'bj': (11, 110)}
    default_city = 'sh'

    same_digit = 4
    display_digit = 4
    display_same_digit = 5

    def __init__(self, default_city=''):
        if default_city in ['bj', 'sh']:
            self.default_city = default_city

    def get_data(self, city='sh'):
        url = self.__URL % self.__city_code[city]
        request = UrlRequest(url)
        self.phone_data = request.get_request()
        regex = ur"1[3-8]\d{9}"
        self.phone_list = re.findall(regex, self.phone_data)

    def send_mail(self, msg):
        mail = SMTPMail('pz_mail@126.com', '168901')
        mail.from_mail = 'pz_mail@126.com'
        mail.to_mail = 'pengzhan@mesway.com'
        mail.subject = 'Phone list'
        mail.message = msg
        mail.send()

    def run(self):
        self.get_data(self.default_city)
        phone_rule = PhoneRule(self.phone_list)
        same_digit_phone = phone_rule.same_digit(self.same_digit)
        display_digit_phone = phone_rule.display_digit(self.display_digit)
        display_same_digit_phone = phone_rule.display_same_digit(self.display_same_digit)
        if len(display_same_digit_phone) or (len(same_digit_phone) and len(display_digit_phone)):
            msg = "same digit: "+repr(same_digit_phone)
            msg += "<br /> display digit: " + repr(display_digit_phone)
            self.send_mail(msg)
            print msg


if __name__ == "__main__":
    phone = Phone()
    phone.run()
    phone.default_city = 'bj'
    phone.run()
